import { Injectable } from '@angular/core';
import * as AlfrescoApi from 'alfresco-js-api';


@Injectable({
  providedIn: 'root'
})
export class AlfrescoService {
  private alfrescoApi: AlfrescoApi;

  constructor() {
    // this.alfrescoApi = new AlfrescoApi({
    //   provider: 'ECM',
    //   hostEcm: 'http://192.168.15.202:8080',
    //   authType: 'BASIC',
    //   contextRoot: 'alfresco'
    // });
    console.log("iniciando constructor");
    

    this.alfrescoApi = new AlfrescoApi({ provider: 'ECM', hostEcm: 'http://192.168.15.202:8080' });

   

  }

  async getDirectories(): Promise<any[]> {
    await this.alfrescoApi.login('user', 'user').then(function (data) {
      console.log('API called successfully to login into Alfresco Content Services.');

    }, function (error) {
      console.log(error);
    });

    const response = await this.alfrescoApi.core.nodesApi.getNodeChildren('-root-', {
      where: `(isFolder=true)`
    });
    return response.list.entries;
  }

  async getDocumentsInFolder(folderId: string): Promise<any[]> {
    const response = await this.alfrescoApi.nodes.getNodeChildren(folderId);
    return response.list.entries;
  }
}
