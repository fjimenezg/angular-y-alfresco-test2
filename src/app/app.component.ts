import { Component, OnInit } from '@angular/core';
import { AlfrescoService } from './alfresco.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  folders: any[];
  documents: any[];

  constructor(private alfrescoService: AlfrescoService) {}
  
  ngOnInit(): void {
    console.log("iniciando ONinit");
    
    this.alfrescoService.getDirectories().then((folders) => {
      this.folders = folders;
      console.log("folders:", folders);
      
    });
  }

  listar(folderId:any){
   
    this.alfrescoService.getDocumentsInFolder(folderId).then((documents) => {
      this.documents = documents;
      console.log("documents",documents);
      
    });
  }

}
